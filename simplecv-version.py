# coding=utf-8
from __future__ import division
import time
from SimpleCV import Display, DrawingLayer, Color, VirtualCamera, Image, Camera

# Init
previewContent = 'img'  # Rendered Layer : img, black, redDistance, greenDistance, redBinary, greenBinary

###################################################################
# Faux positifs détectés (pulls, bonnets, ...)

redObjects = 0

greenObjects = 0

##################################################################

# Source
source = 'Image'  # Image, Camera
cameraIndex = 5  # 0 on mac, 5 on PC

sourceWidth = 1280
sourceHeight = 720

testImagePath = 'C:/Users/nico/Documents/simplecv/files/snap.png'

# Color Detection Values
redHue = 180
redSaturation = 160
redValue = 60

greenHue = 80
greenSaturation = 120
greenValue = 60

binarizeThreshold = 180

blobMinSize = 200
rectDistance = 0.10

# Preview dimensions
imgWidth = 1280
imgHeight = 720

statsHeight = 70  # Percentage of the picture height

barsWidth = 40

# Performance
waitTime = 0  # Wait between frames, float seconds, 0 for full speed

# Error management
retryOnError = 50  # Number of retries before quit

# Connect Source
if source == 'Camera':
    cam = Camera(camera_index=cameraIndex, prop_set={"width": sourceWidth, "height": sourceHeight})
else:
    cam = VirtualCamera(testImagePath, "image")

# Init Displays
display = Display((imgWidth, imgHeight))

failCount = 0
while display.isNotDone() and failCount < retryOnError:

    if waitTime > 0:
        time.sleep(waitTime)

    if display.lastLeftButton:
        break

    try:
        img = cam.getImage()
    except IOError:
        failCount += 1
        print "GetImage retry ", failCount
        continue

    failCount = 0

    if sourceWidth != imgWidth or source == 'Image':
        img = img.resize(imgWidth, imgHeight)

    img = img.blur()

    # Hue Distance
    redDistance = img.hueDistance(redHue, redSaturation, redValue)
    greenDistance = img.hueDistance(greenHue, greenSaturation, greenValue)

    # Binarize
    redBinary = redDistance.binarize(binarizeThreshold)
    greenBinary = greenDistance.binarize(binarizeThreshold)

    # Detect Blobs
    redBlobs = redBinary.findBlobs(-1, blobMinSize)
    # redBlobs = redBlobs.filter([b.isRectangle(rectDistance) for b in redBlobs])

    greenBlobs = greenBinary.findBlobs(-1, blobMinSize)
    # greenBlobs = greenBlobs.filter([b.isRectangle(rectDistance) for b in greenBlobs])

    if redBlobs:
        redBlobs.draw(color=Color.RED, width=2)
        redCount = len(redBlobs)
        img.addDrawingLayer(redBinary.dl())
    else:
        redCount = 0

    if greenBlobs:
        greenBlobs.draw(color=Color.GREEN, width=3)
        greenCount = len(greenBlobs)
        img.addDrawingLayer(greenBinary.dl())
    else:
        greenCount = 0

    # Remove false positives
    if redCount >= redObjects:
        redCount -= redObjects

    if greenCount >= greenObjects:
        greenCount -= greenObjects

    # Calc proportion
    total = redCount + greenCount

    if total > 0:
        redPrc = int((redCount / total) * 100)
        greenPrc = 100 - redPrc

        redOut = str(redPrc) + " %"
        greenOut = str(greenPrc) + " %"
    else:
        redPrc = 0
        greenPrc = 0
        redOut = "-"
        greenOut = "-"

    # Background Image
    if previewContent == 'redDistance':
        img = redDistance
    elif previewContent == 'greenDistance':
        img = greenDistance
    elif previewContent == 'redBin':
        img = redBinary
    elif previewContent == 'greenBin':
        img = greenBinary
    elif previewContent == 'black':
        img = Image((imgWidth, imgHeight))

    # Background Stats Bloc
    popHeight = int(imgHeight * statsHeight / 100)
    pop = (250, imgHeight)

    backGround = DrawingLayer(pop)
    backGround.rectangle((0, 0), pop, Color.BLACK, 1, True)
    img.addDrawingLayer(backGround)

    # Write text
    textLayer = DrawingLayer(pop)

    textLayer.selectFont('arial')  # arial, arialblack, arialrounded # textLayer.listFonts()
    textLayer.setFontBold(True)
    textLayer.setFontSize(44)

    textLayer.text(redOut, (20, 20), color=Color.WHITE)
    textLayer.text(greenOut, (145, 20), color=Color.WHITE)

    img.addDrawingLayer(textLayer)

    # bloc count
    countLayer = DrawingLayer(pop)

    countLayer.setFontSize(22)

    countLayer.text(str(redCount), (40, imgHeight - 30), color=Color.RED)
    countLayer.text(str(greenCount), (165, imgHeight - 30), color=Color.GREEN)

    img.addDrawingLayer(countLayer)

    # bars
    barHeight = popHeight - 130
    barY = barHeight + 100
    barRX = 62
    barGX = 187

    redHeight = int(redPrc / 100 * barHeight)
    greenHeight = int(greenPrc / 100 * barHeight)

    barsLayer = DrawingLayer((imgWidth, imgHeight))
    barsLayer.line((barRX, barY), (barRX, barY - redHeight), Color.RED, width=barsWidth)
    barsLayer.line((barGX, barY), (barGX, barY - greenHeight), Color.GREEN, width=barsWidth)

    img.addDrawingLayer(barsLayer)

    img.save(display)
