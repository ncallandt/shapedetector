#!/usr/bin/env python3

import cv2

from helpers.Helpers import Convi

convi = Convi()

convi.init_capture()
convi.init_windows()

while 1:

    convi.set_frame(blurred=True)
    convi.color_detection()

    convi.show_windows()

    # abord on ESC
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

    convi.get_sliders()
    # Read Sliders

convi.dump_config()
cv2.destroyAllWindows()
