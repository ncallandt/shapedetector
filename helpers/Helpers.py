import argparse
import yaml
import os
import cv2
import numpy as np
import shutil


class Convi:
    def __init__(self):
        # Init
        self.frame = None
        self.blured_frame = None
        self.img_frame = None
        self.red_mask = None
        self.green_mask = None
        self.render_frame = None

        # Arguments
        self.argv = self.parse_arguments()

        # Configuration
        self.config_path = None
        self.config = self.load_config()

    def load_config(self):
        config = None
        default_path = os.path.realpath('configs/default.yml')
        config_path = os.path.realpath('configs/' + self.argv['configname'] + '.yml')

        if not os.path.isfile(config_path):
            shutil.copy2(default_path, config_path)
            print("Config Created : " + config_path)

        if os.path.isfile(config_path):
            with open(config_path, 'r') as stream:
                config = yaml.safe_load(stream)
            self.config_path = config_path
            print("Using config : " + config_path)
        else:
            print('ERR : Couldn\'t load configuration at : ' + config_path)
            exit(5)

        return config

    @staticmethod
    def parse_arguments():
        ap = argparse.ArgumentParser()

        ap.add_argument("-m", "--mode", dest='mode', type=str, default="camera",
                        help="Capture mode : camera, video, image")
        ap.add_argument("-f", "--file", dest='file', type=str, help="Video/Image file path when using video capture")
        ap.add_argument("-c", "--calibration", dest='calibration', type=str,
                        help="Calibrate parameters : green, red, blobs")
        ap.add_argument("-n", "--config", dest='configname', type=str, default='default',
                        help="Name to use for the configuration file")

        return vars(ap.parse_args())

    def init_capture(self):
        if self.argv['mode'] == "camera":
            self.cap = cv2.VideoCapture(self.config['cameraId'])
            self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.config['cameraWidth'])
            self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self.config['cameraHeight'])

        elif self.argv['mode'] == "video":
            self.cap = cv2.VideoCapture(self.argv['file'])

        elif self.argv['mode'] == "image":
            self.img_frame = cv2.imread(self.argv['file'])

    def set_frame(self, blurred):

        frame = None

        if self.argv['mode'] == "camera" or self.argv['mode'] == "video":
            frame = self.cap.read()[1]

        elif self.argv['mode'] == "image":
            frame = self.img_frame

        if blurred:
            frame = cv2.medianBlur(frame, self.config['blurRadius'])

        # frame = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_CUBIC)

        self.frame = frame

    def init_windows(self):
        cv2.namedWindow('Image', cv2.WINDOW_OPENGL)

        if self.argv['calibration'] == 'red':
            self.create_trackbars('red')

        elif self.argv['calibration'] == 'green':
            self.create_trackbars('green')

        elif self.argv['calibration'] == 'blobs':
            cv2.namedWindow('Settings', cv2.WINDOW_NORMAL)
            # cv2.createTrackbar('minThreshold', 'Settings', self.config['blobs']['minThreshold'], 500, self.nothing)
            # cv2.createTrackbar('maxThreshold', 'Settings', self.config['blobs']['maxThreshold'], 500, self.nothing)
            cv2.createTrackbar('minArea', 'Settings', self.config['blobs']['minArea'], 1000, self.nothing)
            # cv2.createTrackbar('minCircularity', 'Settings', self.config['blobs']['minCircularity'], 100, self.nothing)
            cv2.createTrackbar('minConvexity', 'Settings', self.config['blobs']['minConvexity'], 100, self.nothing)
            cv2.resizeWindow('Settings', 800, 200)

    def show_windows(self):
        width = self.config['displayWidth']
        height = round(width / 16 * 9)

        self.render_frame = cv2.resize(self.render_frame, (width, height), interpolation=cv2.INTER_AREA)
        cv2.imshow('Image', self.render_frame)

    def extract_color(self, hue):

        min_color = np.array([hue['hue'] - hue['width'], hue['min'], hue['min']], np.uint8)
        max_color = np.array([hue['hue'] + hue['width'], hue['max'], hue['max']], np.uint8)

        hsv_img = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)

        return cv2.inRange(hsv_img, min_color, max_color)

    def get_sliders(self):
        if self.argv['calibration'] == 'red':
            self.read_trackbars('red')

        elif self.argv['calibration'] == 'green':
            self.read_trackbars('green')

        elif self.argv['calibration'] == 'blobs':
            # self.config['blobs']['maxThreshold'] = cv2.getTrackbarPos('maxThreshold', 'Settings')
            self.config['blobs']['minArea'] = cv2.getTrackbarPos('minArea', 'Settings')
            # self.config['blobs']['minCircularity'] = cv2.getTrackbarPos('minCircularity', 'Settings')
            self.config['blobs']['minConvexity'] = cv2.getTrackbarPos('minConvexity', 'Settings')
            # self.config['blobs']['minThreshold'] = cv2.getTrackbarPos('minThreshold', 'Settings')

    def nothing(self, value):
        pass

    def color_detection(self):
        self.red_mask = self.extract_color(self.config['red'])
        self.green_mask = self.extract_color(self.config['green'])

        if self.argv['calibration'] == 'red':
            self.render_frame = cv2.bitwise_and(self.frame, self.frame, mask=self.red_mask)
        elif self.argv['calibration'] == 'green':
            self.render_frame = cv2.bitwise_and(self.frame, self.frame, mask=self.green_mask)
        elif self.argv['calibration'] == 'blobs':
            both_mask = cv2.bitwise_not(self.red_mask, self.red_mask, mask=self.green_mask)
            self.render_frame = cv2.bitwise_and(self.frame, self.frame, mask=both_mask)
            self.detect_blobs()
        else:
            self.render_frame = self.frame
            self.detect_blobs()

    def dump_config(self):
        with open(self.config_path, 'w') as outfile:
            yaml.dump(self.config, outfile, default_flow_style=False)

    def create_trackbars(self, color_name):
        cv2.namedWindow('Settings', cv2.WINDOW_NORMAL)
        cv2.createTrackbar('Threshold', 'Settings', self.config[color_name]['hue'], 180, self.nothing)
        cv2.createTrackbar('Width', 'Settings', self.config[color_name]['width'], 10, self.nothing)
        cv2.createTrackbar('Min', 'Settings', self.config[color_name]['min'], 255, self.nothing)
        cv2.createTrackbar('Max', 'Settings', self.config[color_name]['max'], 255, self.nothing)
        cv2.resizeWindow('Settings', 800, 400)

    def read_trackbars(self, color_name):
        self.config[color_name]['hue'] = cv2.getTrackbarPos('Threshold', 'Settings')
        self.config[color_name]['width'] = cv2.getTrackbarPos('Width', 'Settings')
        self.config[color_name]['min'] = cv2.getTrackbarPos('Min', 'Settings')
        self.config[color_name]['max'] = cv2.getTrackbarPos('Max', 'Settings')

    def detect_blobs(self):

        params = cv2.SimpleBlobDetector_Params()

        # Change thresholds
        # params.minThreshold = self.config['blobs']['minThreshold']
        # params.maxThreshold = self.config['blobs']['maxThreshold']

        # Filter by Area.
        params.filterByArea = True
        params.minArea = self.config['blobs']['minArea']

        # Filter by Circularity
        params.filterByCircularity = False
        # params.minCircularity = self.config['blobs']['minCircularity'] / 100

        # Filter by Convexity
        params.filterByConvexity = True
        params.minConvexity = self.config['blobs']['minConvexity'] / 100

        # Filter by Inertia
        params.filterByInertia = False
        # params.minInertiaRatio = self.config['blobs']['minInertiaRatio'] / 100

        detector = cv2.SimpleBlobDetector_create(params)

        # Detect blobs.
        red_blobs = detector.detect(255 - self.red_mask)
        green_blobs = detector.detect(255 - self.green_mask)

        # Draw detected blobs as red circles.
        self.render_frame = cv2.drawKeypoints(self.render_frame, red_blobs, np.array([]), (0, 0, 255),
                                              cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        self.render_frame = cv2.drawKeypoints(self.render_frame, green_blobs, np.array([]), (0, 255, 0),
                                              cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
